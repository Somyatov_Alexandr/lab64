import React, {Component} from 'react';
import {NavLink, Route, Switch} from "react-router-dom";
import './App.css';
import PostsList from "./containers/PostsList/PostsList";
import PostAdd from "./containers/PostAdd/PostAdd";
import PostFull from "./containers/PostFull/PostFull";
import PostEdit from "./containers/PostEdit/PostEdit";
import About from "./containers/About/About";

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <header className="header">
          <div className="logo">
            <img src="img/logo.gif" alt="My blog"/>
          </div>
          <menu className="top-nav">
            <li className="top-nav__item">
              <NavLink to="/" exact>Home</NavLink>
            </li>
            <li className="top-nav__item">
              <NavLink to="/add" exact>Add</NavLink>
            </li>
            <li className="top-nav__item">
              <NavLink to="/about" exact>About</NavLink>
            </li>
            <li className="top-nav__item">
              <NavLink to="/contacts" exact>Contacts</NavLink>
            </li>
          </menu>
        </header>

        <Switch>
          <Route path="/" exact component={PostsList} />
          <Route path="/add" exact component={PostAdd} />
          <Route path="/edit" exact component={PostEdit} />
          <Route path="/posts/:id" component={PostFull}/>
          <Route path="/about" exact component={About} />
          <Route render={() => <h1>Not found</h1>}/>
        </Switch>
      </div>
    );
  }
}

export default App;
