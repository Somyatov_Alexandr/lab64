import React, {Component} from 'react';
import axios from 'axios';
import PostItem from "../../components/PostItem/PostItem";
import Loading from "../../components/UI/Loading/Loading";

class PostsList extends Component {
  state = {
    posts: [],
    loading: false
  };

  componentDidMount(){
    this.setState({loading: true});
    axios.get('/posts.json').then(response =>{
      const posts = [];

      for (let key in response.data) {
        posts.push({...response.data[key], id: key});
      }

      this.setState({posts, loading: false});
    })
  }



  render () {
    if (!this.state.loading) {
      return (
          <div className="post__list">
            {this.state.posts.map(post => (
                <PostItem
                    key={post.id}
                    title={post.title}
                    author={post.author}
                    body={post.body}
                    datetime={post.datetime}
                    id={post.id}
                />
            ))}
          </div>
      );
    } else {
      return <Loading/>
    }
  }
}

export default PostsList;