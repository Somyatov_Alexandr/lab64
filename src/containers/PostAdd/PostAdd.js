import React, {Component} from 'react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import axios from 'axios';
import {Editor} from "react-draft-wysiwyg";
import { EditorState } from 'draft-js';
import { convertToHTML } from 'draft-convert';

class PostAdd extends Component {
    state = {
        post: {
            title: '',
            author: '',
            body: '',
            datetime: null
        },
        editorState: EditorState.createEmpty(),
        loading: false
    };

    static getTimeToString(){
        const date = new Date();
        const values = [ date.getDate(), date.getMonth() + 1 ];
        for( let id in values ) {
            values[id] = values[ id ].toString().replace( /^([0-9])$/, '0$1' );
        }
        return values[0] + '.' + values[1] + '.' + date.getFullYear();
    }

    changePostHandler = (event) => {
        const newPost = {...this.state.post};
        const key = event.target.name;
        const value = event.target.value;
        const date = PostAdd.getTimeToString();
        newPost[key] = value;
        newPost.datetime = date;
        this.setState({post: newPost});
    };

    onEditorStateChange = (editorState) => {
        const post = {...this.state.post};
        post.body = convertToHTML(editorState.getCurrentContent());
        this.setState({editorState, post});
    };



    addPostHandler = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        axios.post('/posts.json', this.state.post).then(response => {
            this.setState({loading: false});
        }).finally(() => {
            this.props.history.push('/');
        });
    };


    render () {

        return (
            <div className="post-add item-plank">
                <h1 className="post-add__heading">Add new post</h1>
                <form className="form-edit">
                    <div className="form-group form__main-info">
                        <input
                            className="form-control"
                            type="text" name="title"
                            placeholder="Enter title"
                            onChange={(event) => this.changePostHandler(event)}/>
                        <input
                            className="form-control"
                            type="text" name="author"
                            placeholder="Enter author name"
                            onChange={(event) => this.changePostHandler(event)}/>
                    </div>
                    <div className="form-group-editor">
                        <Editor
                            editorState={this.state.editorState}
                            toolbarClassName="toolbarEditor"
                            wrapperClassName="wrapperEditor"
                            editorClassName="editor"
                            onEditorStateChange={this.onEditorStateChange}
                        />
                    </div>
                    <button className="btn btn__save" onClick={this.addPostHandler}>Save</button>
                </form>
            </div>
        );
    }
}

export default PostAdd;