import React, {Component} from 'react';
import axios from 'axios';
import Loading from "../../components/UI/Loading/Loading";

class PostFull extends Component {

    state = {
        currentPost: null
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/posts/${id}.json`).then(response =>{
            this.setState({currentPost: response.data})
        })
    };

    removePost = () => {
        const id = this.props.match.params.id;
        axios.delete(`/posts/${id}.json`).finally(() => {
            this.props.history.push('/');
        });
    };


    showEditForm = () => {
        const postParams = [];
        const id = this.props.match.params.id;
        for (let key in this.state.currentPost) {
            postParams.push(encodeURIComponent(key) + '=' + encodeURIComponent(this.state.currentPost[key]));
        }
        postParams.push('id='+ id);
        const postParamsString = postParams.join('&');

        this.props.history.push({
            pathname: '/edit',
            search: '?' + postParamsString
        });
    };

    render () {

        if (this.state.currentPost) {
            return (
                <div className="post item-plank">
                    <h1 className="post__heading">{this.state.currentPost.title}</h1>
                    <div className="post__info">
                      <div className="post__create"><span className="post_bold">Created on:</span> {this.state.currentPost.datetime}</div>
                      <div className="post__author"><span className="post_bold">Author:</span> {this.state.currentPost.author}</div>
                    </div>
                    <div className="post__text" dangerouslySetInnerHTML={{ __html: this.state.currentPost.body }} />
                    <div className="controls">
                        <button className="btn post__remove danger btn__small" onClick={this.removePost}>Удалить</button>
                        <button className="btn post__edit btn__small" onClick={this.showEditForm}>Редактировать</button>
                    </div>
                </div>
            )
        } else {
            return(
                <Loading/>
            )
        }
    }
}

export default PostFull;