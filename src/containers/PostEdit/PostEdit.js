import React, {Component} from 'react';
import './PostEdit.css';
import axios from 'axios';
import {Editor} from "react-draft-wysiwyg";
import {EditorState} from "draft-js";
import {convertToHTML, convertFromHTML} from "draft-convert";

class PostEdit extends Component {
    state = {
        post: {
            title: '',
            author: '',
            body: '',
            datetime: null
        },
        editorState: EditorState.createEmpty(),
        loading: false
    };

    changePostHandler = (event) => {
        const newPost = {...this.state.post};
        const key = event.target.name;
        const value = event.target.value;
        newPost[key] = value;
        this.setState({post: newPost});
    };

    onEditorStateChange = (editorState) => {
        const post = {...this.state.post};
        post.body = convertToHTML(editorState.getCurrentContent());
        this.setState({editorState, post});
    };

    editPostHandler = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        const id = this.state.post.id;
        const updatePost = {
            title: this.state.post.title,
            author: this.state.post.author,
            body: this.state.post.body,
            datetime: this.state.post.datetime
        };
        axios.patch(`/posts/${id}.json`, updatePost).then(response => {
            this.setState({loading: false});
        }).finally(() => {
            this.props.history.push(`/posts/${id}`);
        });
    };

    showPostParams = () => {
        const postParams = new URLSearchParams(this.props.location.search);
        let editorState = null;
        const post = {};

        for (let param of postParams.entries()) {
            if (param[0] === 'body') {
                editorState = EditorState.createWithContent(convertFromHTML(param[1]));
            } else {
                post[param[0]] = param[1];
            }
        }
        this.setState({post, editorState});
    };

    componentDidMount(){
        if (this.props.location.search !== ""){
            this.showPostParams();
        }
    }

    render () {

        return (
            <div className="post-add item-plank">
                <h1 className="post-add__heading">Edit post</h1>
                <form className="form-edit">
                    <div className="form-group form__main-info">
                      <input
                          className="form-control"
                          type="text"
                          name="title"
                          value={this.state.post.title}
                          onChange={(event) => this.changePostHandler(event)}/>
                      <input
                          className="form-control"
                          type="text"
                          name="author"
                          value={this.state.post.author}
                          onChange={(event) => this.changePostHandler(event)}/>
                    </div>
                    <div className="form-group-editor">
                        <Editor
                            editorState={this.state.editorState}
                            toolbarClassName="toolbarEditor"
                            wrapperClassName="wrapperEditor"
                            editorClassName="editor"
                            onEditorStateChange={this.onEditorStateChange}
                        />
                    </div>
                    <button className="btn btn__save" onClick={this.editPostHandler}>Save</button>
                </form>

            </div>
        );
    }
}

export default PostEdit;