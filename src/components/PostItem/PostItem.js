import React from 'react';
import './PostItem.css';
import {Link} from "react-router-dom";


const PostItem = props => {
  return (
      <div className="post-item item-plank">
        <h3 className="post-item__heading">{props.title}</h3>
        <div className="post__info">
          <div className="post__create"><span className="post_bold">Created on:</span> {props.datetime}</div>
          <div className="post__author"><span className="post_bold">Author:</span> {props.author}</div>
        </div>
        <div className="post__text" dangerouslySetInnerHTML={{ __html: props.body }} />
        <Link className="btn readmore__btn btn__small" to={`/posts/${props.id}`}>Read more</Link>
      </div>
  );
};

export default PostItem;