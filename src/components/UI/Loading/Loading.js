import React from 'react';
import './Loading.css';

const Loading = () => {
  return (
      <div className="cssload-jumping">
        <span/><span/><span/><span/><span/>
      </div>
  );
};

export default Loading;